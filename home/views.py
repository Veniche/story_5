from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView
from .forms import MatkulForm
from .models import Matkul
from django.urls import reverse_lazy

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def about(request):
    return render(request, 'home/about.html')

def project(request):
    return render(request, 'home/project.html')

class MatkulView(ListView):
    model = Matkul
    template_name = 'home/list.html'

class MatkulDetailView(DetailView):
    model = Matkul
    template_name = 'home/detail.html'

class MatkulDeleteView(DeleteView):
    model = Matkul
    template_name = 'home/delete.html'
    success_url = reverse_lazy('home:list')

def Matkul_create_view(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = MatkulForm()
    context = {
        "form": form
    }
    return render(request, 'home/matkul_create.html', context)

def Matkul_detail_view(request):
    obj = Matkul.objects.get(id=1)
    context = {
        'object': obj
    }
    return render(request, 'home/matkul_create.html', context)