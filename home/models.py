from django.db import models

# Create your models here.
class Matkul(models.Model):
    NamaMatkul = models.CharField(max_length=50)
    Dosen = models.CharField(max_length=50)
    sks = models.IntegerField(blank=True, null=True)
    deskripsi = models.CharField(max_length=100)
    smst = models.CharField(max_length=15)
    kelas = models.CharField(max_length=50)