from django.urls import path
from . import views
from .views import MatkulView, MatkulDetailView, MatkulDeleteView

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('project', views.project, name='project'),
    path('matkul_create', views.Matkul_create_view, name='matkul_create'),
    path('list', MatkulView.as_view(), name='list'),
    path('detail/<int:pk>', MatkulDetailView.as_view(), name='detail'),
    path('detail/<int:pk>/delete', MatkulDeleteView.as_view(), name='delete'),
]